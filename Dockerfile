FROM riscv64/ubuntu:lunar as beagle/ubuntu-riscv64-lunar-beaglev
ENV DEBIAN_FRONTEND=noninteractive
RUN apt-get update \
    && apt-get install -y apt-utils \
    && rm -rf /var/lib/apt/lists/*
RUN apt-get update \
    && apt-get install -y locales \
	&& localedef -i en_US -c -f UTF-8 -A /usr/share/locale/locale.alias en_US.UTF-8 \
    && rm -rf /var/lib/apt/lists/*
ENV LANG en_US.utf8
RUN echo "deb [trusted=yes] http://repos.rcn-ee.net/debian-riscv64 lunar main" > /etc/apt/sources.list.d/beagle.list \
    && echo "deb-src [trusted=yes] http://repos.rcn-ee.net/debian-riscv64 lunar main" >> /etc/apt/sources.list.d/beagle.list \
    && apt-get update \
    && apt-get install -yq bbb.io-keyring \
    && echo "deb [arch=riscv64 signed-by=/usr/share/keyrings/rcn-ee-archive-keyring.gpg] http://repos.rcn-ee.net/debian-riscv64 lunar main" > /etc/apt/sources.list.d/beagle.list \
    && echo "deb-src [arch=riscv64 signed-by=/usr/share/keyrings/rcn-ee-archive-keyring.gpg] http://repos.rcn-ee.net/debian-riscv64 lunar main" >> /etc/apt/sources.list.d/beagle.list \
    && rm -rf /var/lib/apt/lists/*
RUN apt-get update \
    && apt-get install -y \
	alsa-utils	\
	avahi-utils	\
	automake	\
	bash-completion	\
	bc	\
	bison	\
	bluetooth	\
	bluez-firmware	\
	bsdmainutils	\
	btrfs-progs	\
	build-essential	\
	ca-certificates	\
	can-utils	\
	cloud-guest-utils	\
	command-not-found	\
	curl	\
	device-tree-compiler	\
	docker.io	\
	dosfstools	\
	file	\
	flex	\
	git	\
	gnupg	\
	gpiod	\
	hexedit	\
	hostapd	\
	htop	\
	i2c-tools	\
	initramfs-tools	\
	iperf3	\
	iw	\
	less	\
	libgpiod-dev	\
	libiio-dev	\
	libiio-utils	\
	libinline-files-perl	\
	libncurses5-dev	\
	libnss-mdns	\
	libnss-systemd	\
	libpam-systemd	\
	libssl-dev	\
	libtool	\
	linux-base	\
	locales	\
	nano	\
	ncdu	\
	net-tools	\
	nginx	\
	openssh-server	\
	pastebinit	\
	patch	\
	pkg-config	\
	python-is-python3	\
	python3-dev	\
	python3-libgpiod	\
	python3-pip	\
	python3-pycryptodome	\
	python3-pyelftools	\
	python3-setuptools	\
	python3-websockify	\
	rsync	\
	snapd	\
	ssl-cert	\
	strace	\
	systemd-timesyncd	\
	sudo	\
	tio	\
	tree	\
	u-boot-tools	\
	unzip	\
	usb-modeswitch	\
	usbutils	\
	v4l-utils	\
	vim	\
	wget	\
	wireguard-tools	\
	wireless-tools	\
	wpasupplicant	\
	xinput	\
	zstd	\
	lmbench	\
	man-db	\
	net-tools	\
	python3-opencv	\
    && rm -rf /var/lib/apt/lists/* 
ENTRYPOINT ["/bin/bash"]
